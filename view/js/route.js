import { InitPage,InitDirTag } from "./MediaPlayer.js";
const body = document.querySelector('body');
body.addEventListener('click', (e) => {
    const link = e.target.closest('a');
    if (link) {
        // 点击到了 A 元素或其子元素
        // 在这里编写处理代码
        const href = link.href;
        //判断是否为目录或者文件夹
        if (href.endsWith('/') || href.substring(href.lastIndexOf('/') + 1).indexOf('.') == -1) {
            if (href !== location.href.replace(location.pathname,"/")) {
                e.preventDefault();
                //修改地址栏，但不刷新页面
                //    history.pushState(null, null, href);
                  //console.log(href.replace(location.origin,"http://localhost:3000"));
                   fetch(href.replace(location.origin,"http://localhost:3000"))
                   .then(res => res.json()).then(InitPage);
                    InitDirTag(href.replace(location.origin,""));
            }
            
        }else{
            e.preventDefault();
            //修改地址栏，但不刷新页面
            //    history.pushState(null, null, href);
              //console.log(href.replace(location.origin,"http://localhost:3000"));
               fetch(href.replace(location.origin,"http://localhost:3000"))
               .then(res => res.blob()).then(blob=>{
                //显示图片
                const img = document.createElement('img');
                img.src = URL.createObjectURL(blob);
                img.onload = function() {
                    URL.revokeObjectURL(img.src); // 清除释放
                }
                body.innerHTML = '';
                body.appendChild(img);
                
               });
               
            return;
        }   
    }
});


