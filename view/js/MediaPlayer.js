'use strict';

/**
 * 
 * @param {string} href 
 * @returns 
 */
export function getClass(href) {
    if (href.endsWith('/') || href.substring(href.lastIndexOf('/') + 1).indexOf('.') == -1) {
        return "icon icon-directory";
    } else {
        const fileExtension = href.split('.').pop().toLowerCase();
        switch (fileExtension) {
            case 'pdf':
                return 'icon icon-pdf';
            case 'doc':
            case 'docx':
                return 'icon icon-word';
            case 'xls':
            case 'xlsx':
                return 'icon icon-excel';
            case 'ppt':
            case 'pptx':
                return 'icon icon-powerpoint';
            case 'zip':
            case 'rar':
                return 'icon icon-archive';
            case 'mp3':
            case 'wav':
                return 'icon icon-audio';
            case 'mp4':
            case 'avi':
            case 'mov':
                return 'icon icon-video';
            case 'jpg':
            case 'png':
            case 'gif':
                return 'icon icon-image';
            default:
                return 'icon icon-file';
        }
    }
}

export function InitPage(res) {
    if (res instanceof Array && res.length > 0) {
        let model_lis = "";
        for (let item of res) {
          
          model_lis += `
        <li>
            <a href="/${item.isDirectory?'readdir':'file'}${item.path+item.name}" class="${getClass(item.name)}" ><span class="name">${item.name}</span></a>
        </li>
        `;
        }

        const ul = document.querySelector('ul'); // modify this line to select the ul element
        ul.innerHTML = model_lis;
}
}
/**
 * 
 * @param {string} path 
 */
export function InitDirTag(path){
    path = path.endsWith('/')?path.substring(0,path.length-1):path;
    let dirList = path.replace("/readdir","").split("/");
    const dirTag = document.querySelector('#wrapper').querySelector("h1");
    let  dirTagInnerHTML = "";
    for(let dir of dirList){
        dirTagInnerHTML += `<a href="/readdir${'/'+dir}">${dir||'~'}</a>/`;
    }
    dirTag.innerHTML = dirTagInnerHTML;
}
