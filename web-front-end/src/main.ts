import "./assets/normalize.css";
import { createApp } from 'vue'
import ElementPlus  from "element-plus"
import "element-plus/dist/index.css";
import Router from "./routes/index";
import App from './App.vue';

createApp(App)
.use(ElementPlus,{size:"small"})
.use(Router)
.mount('#app')
