import {post,get} from "../../base";

//login
export const login = (data: 
    { email: string,password: string} 
  | {phone: string,password: string}
) => post('/user/login', data);

//register
export const register = (data: 
    { email: string,password: string} 
    | {phone: string,password: string}
    ) => post('/user/register', data);
//refresh token
export const refreshToken = () => get('/user/refreshToken');