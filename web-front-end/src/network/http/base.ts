'use stract'
import axios from "axios";
import config from "./config";
axios.defaults.baseURL =config.baseURL;
axios.defaults.timeout = config.timeout;

const token = localStorage.getItem('token');
const refreshToken = localStorage.getItem('refreshToken');

axios.interceptors.request.use(
    config => {
        if (token) {
            config.headers.Authorization = 'Bearer ' + token;
        }
        if (refreshToken) {
            config.headers['x-refresh-token'] = refreshToken;
        }
        return config;
    }
    , error => {
        if (error.response && error.response.status === 401){
            if (token && refreshToken) {
                axios.get('/user/refreshToken').then(res => {
                    localStorage.setItem('token', res.data.token);
                    localStorage.setItem('refreshToken', res.data.refreshToken);
                });
            }else{
                //跳转到登录页面
                location.href = '/login';
            }
        }else{
            //分析错误原因
            //是否是网络问题
            if (window.navigator.onLine) {
                //提示网络异常
                console.log('网络异常');
                Promise.reject(error);
            }
            //是否是服务器问题
            if (error.response && error.response.status >= 500) {
                
            }
            //是否是客户端问题
            if (error.response && error.response.status >= 400) {
                
            }
        }
        return Promise.reject(error);
    });


export const get = axios.get;

export const post = axios.post;

export const put = axios.put;

export const del = axios.delete;

export const axiosClient = axios;