'use strict'
import  {jwtDecode} from 'jwt-decode';

const removeToken = ()=>{
    localStorage.removeItem('token');
};
const removeRefreshToken = ()=>{
    localStorage.removeItem('refreshToken');
};
export const TokenHelper = {
    get token():string | null{
        return localStorage.getItem('token');
    } ,
    set token(token:string){
        console.log(token);
        
        localStorage.setItem('token',token);
    },
    removeToken
};

export const RefreshTokenHelper = {
    get refreshToken():string | null{
        return localStorage.getItem('refreshToken');
    } ,
    set refreshToken(refreshToken:string){
        console.log(refreshToken);
        
        localStorage.setItem('refreshToken',refreshToken);
    },
    removeRefreshToken
};

export const JwtHelper = {
    decode(token:string){
        return jwtDecode(token) as any;
    }
};
