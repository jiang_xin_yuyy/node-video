export class CustomPromise {
    private callbacks : ((value: any) => void)[] = [];
    private catchCallbacks : ((value: any) => void)[] = [];
    private finallyCallbacks : ((value: any) => void)[] = [];
    then(onFulfilled: (value: any) => void) {
        this.callbacks.push(onFulfilled);
        return this;
    }

    catch(onRejected: (value: any) => void) {
        this.catchCallbacks.push(onRejected);
        return this;
    }

    finally(onFinally: (value: any) => void) {
        this.finallyCallbacks.push(onFinally);
        return this;
    }

    resolve(value:any) {
        this.callbacks.forEach(callback => callback(value));
        this.finallyCallbacks.forEach(callback => callback(value));
    }

    reject(reason:any) {
        this.catchCallbacks.forEach(callback => callback(reason));
        this.finallyCallbacks.forEach(callback => callback(reason));
    }
}