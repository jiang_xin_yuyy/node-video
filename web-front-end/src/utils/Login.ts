import {TokenHelper,JwtHelper} from "./GlobalVariable";
export default {
    get descToken(){
        return JwtHelper.decode(TokenHelper.token||'');
    },
    get IsLogin(){
        if (TokenHelper.token !== null) {
           return true;
        }
        return false;
    },
    get UserName(){
        if (this.descToken) {
            return this.descToken.UserName;
        }
        return null;
    }
}