declare module "*.page.ts"{
    type PageConfig = {
        name:string,
        parent: string,
        path: string,
        icon: string,
        isShow: boolean,
    };
}