const pageConfig ={
    parent: 'Layout',
    icon: 'video',
    name: 'WebRTC',
    path: '/webRTC',
    children: [],
    isShow: true,
};

export default pageConfig;