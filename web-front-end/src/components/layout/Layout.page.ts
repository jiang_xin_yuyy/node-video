export default {
    name: 'Layout',
    parent: 'App',
    path: '/',
    icon: 'el-icon-s-home',
    isShow: true,
};