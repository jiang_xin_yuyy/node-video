const pageConfig ={
    parent: 'Layout',
    icon: 'video',
    name: 'user',
    path: '/user',
    children: [],
    isShow: true,
};

export default pageConfig;