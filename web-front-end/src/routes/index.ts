import routes  from "./routes";
import { createRouter,createWebHistory } from "vue-router";
import loginHelper from "@/utils/Login";
const Router =  createRouter({
    history:createWebHistory(),
    routes: routes
});

//路由守卫
Router.beforeEach((to, from, next) => {
    console.log(to,from);
    
    if (to.path !== '/login' && !loginHelper.IsLogin) {
        next({ path: '/login' });
    } else {
        next();
    }
});


export default Router;