import type { RouteRecordRaw } from 'vue-router';
const components = import.meta.glob('@/components/**/*.vue', { eager: false });
const pageConfig = import.meta.glob('@/components/**/*.page.ts', { eager: true });
//处理路由信息，路由配置需要和目录结构一致且只处理与*.page.ts同名文件
const routes: RouteRecordRaw[] = [];
const pages: {
    path: string, config: {
        name: string,
        parent?: string,
        path: string,
        icon?: string,
        children?: RouteRecordRaw[],
        component?: any
    }
}[] = [];

Object.entries(pageConfig).forEach(([path, page]) => {
    //@ts-ignore
    pages.push({ path: path, config: page.default });
});
let i = 0;
while (pages.length) {
    let page = pages[i];
    page.config.component = components[page.path.replace('.page.ts', '.vue')];
    if (page?.config.parent && page.config.parent !== 'App') {
        const parent = pages.find(item => { return item?.config.name === page?.config.parent });

        if (parent) {
            parent.config.children = parent.config.children || [];
            //@ts-ignore
            parent.config.children.push({ ...page?.config });
        } else {
            pages.push(page);
        }
    }

    if (i === pages.length - 1) {
        while (pages.length) {
            page = pages.shift()!;
            if (page.config.parent === "App") {
                //@ts-ignore
                routes.push({ ...page?.config });
            }
        }
    }
    i++;
}
export default routes;