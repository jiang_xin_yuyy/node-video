import config from "./config";
import { CustomPromise } from "../utils/CustomPromise";
const ProcessMessageList = [] as {
    likeUrl: string,
    customPromise: CustomPromise
}[];
let _client = new WebSocket(config.url);
export const client = _client;
export const consult = recordCustomPromise("/webrtc/consult");
client.onopen = () => {
    console.log('WebSocket Client Connected');
    //发送心跳,记录连接状态和连接用户
    //获取浏览器信息，作为用户信息 （临时解决方案）

    send("/inituser", { user: navigator.userAgent }).then((data) => {
        console.log(data);
    });

};

client.onmessage = (event) => {
    const data = JSON.parse(event.data);
    //判断是否存在likeUrl,存在则执行resolve
    if (ProcessMessageList.find((item) => item.likeUrl === data.likeUrl)) {
        ProcessMessageList.find((item) => item.likeUrl === data.likeUrl)
            ?.customPromise.resolve(data.data);
    }


};


client.onclose = () => {
    console.log('WebSocket Client Closed');
    //连接关闭后执行reject并且清空ProcessMessageList
    ProcessMessageList.forEach((item) => {
        item.customPromise.reject('WebSocket Client Closed');
    });
    ProcessMessageList.splice(0, ProcessMessageList.length);

    //定时重新连接,超过10次则不再连接
    IntervalConnection(1000, 10);
};

//定时重连
function IntervalConnection(timer: number, maxCount: number) {
    let count = 0;
    let a = setInterval(() => {
        _client = new WebSocket(config.url);
        count++;
        if (count > maxCount) {
            clearInterval(a);
        }
    }, timer);
}




export function send(likeUrl: string, data: any) {
    likeUrl = config.url.endsWith('/') ? config.url.replace('/', '') : config.url + likeUrl.startsWith('/') ? likeUrl : '/' + likeUrl;
    //检查send是否在onopen之前执行，如果是则等待onopen执行后再执行send
    let id = (new Date).getDate();
    if (client.readyState === 0) {
        let a = setInterval(() => {
            if (client.readyState === 1) {
                clearInterval(a);
                client.send(JSON.stringify(
                    {
                        likeUrl,
                        id,
                        data,
                    }
                ));
            }
        }, 100);

    } else {
        client.send(JSON.stringify(
            {
                likeUrl,
                id,
                data,
            }
        ));
    }
    return recordCustomPromise(likeUrl);
}

//记录customPromise
function recordCustomPromise(likeUrl: string) {
    let customPromise;
    if (!ProcessMessageList.find((item) => item.likeUrl === likeUrl)) {
        customPromise = new CustomPromise();
        ProcessMessageList.push({ likeUrl, customPromise });
    } else {
        customPromise = ProcessMessageList.find((item) => item.likeUrl === likeUrl)!.customPromise;
    };
    const resCustomPromise = customPromise;
    //返回一个Promise对象，用于接收服务器返回的数据
    return resCustomPromise;
}
