const config = {
    url: 'ws://localhost:1337',
    options: {
        reconnection: true,
        reconnectionDelay: 5000,
        reconnectionAttempts: 10,
    },
}


export default config;