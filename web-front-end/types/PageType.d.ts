import type { RouteRecordRaw } from 'vue-router';

export interface PageConfig extends RouteRecordRaw {
    parent: string, 
    icon: string, 
    name: string,
    path: string,
    isShow: boolean,
    children: pageConfig[]
}