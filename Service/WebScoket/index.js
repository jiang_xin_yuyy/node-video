const WebSocket = require('ws');

const wss = new WebSocket.Server({ port: 3000 });
const users = [];
const routerAction = [
    {
        likeUrl: '/inituser',
        action: (ws, data) => {
            ws.user = data.data.user;
            users.indexOf(data.data.user) === -1 ? users.push(data.data.user) : "";
            //wsList.indexOf(ws) === -1 ? wsList.push(ws) : "";
            //发送在线用户
            let sendData = JSON.stringify({
                likeUrl: '/webrtc/getuser',
                data: users
            });
            wss.clients.forEach((item) => {
                item.send(sendData);
            });
        }
    },
    {
        likeUrl: '/webrtc/getuser',
        action: (ws, data) => {
            data.data = users;
            ws.send(JSON.stringify(data));
        }
    },
    {
        likeUrl: "/webrtc/consult",
        action: (ws, data) => {
            let toUser = data.data.toUser;
            let toWs =[...wss.clients].filter((item) => {
                return item.user === toUser;
            });
            if (toWs.length) {
                toWs[0].send(JSON.stringify(data));
            } else {
                data.data = {
                    type: "error",
                    msg: "对方不在线"
                };
                ws.send(JSON.stringify(data));
            }
        }
    }
];
function getAction(likeUrl) {
    let action = routerAction.filter((item) => {
        return item.likeUrl === likeUrl;
    });
    return action[0].action;
}
wss.on('connection', function connection(ws) {
    ws.on('message', function incoming(message) {
        let data = JSON.parse(message.toString());
        getAction(data.likeUrl)(ws, data);
    });

    ws.on('close', function close() {
        // let index = wsList.indexOf(ws);
        // if (index !== -1) {
        //     wsList.splice(index, 1);
        // }
    });

});
