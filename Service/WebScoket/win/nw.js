let path = require('path');
 
let Service = require('node-windows').Service;
 
// Create a new service object
let svc = new Service({
  name:'node-video-file-service', //名称
  description: '这是node服务器的文件服务',//描述
  script:  path.resolve('../index.js'),//node执行入口
  nodeOptions: [
    '--harmony',
    '--max_old_space_size=4096'
  ]
});

module.exports = svc;