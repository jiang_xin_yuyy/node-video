const sqlite3 = require("sqlite3").verbose();

// open the database
const db = new sqlite3.Database('./Sqlite/node-video.db');

// test the connection
db.serialize(() => {
    db.run('CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT)');
});


// close the database connection
db.close();
