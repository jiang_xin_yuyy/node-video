const { GetFilesOrDir,GetFile } = require("./utils/readFile.js");
const { Path } = require("./utils/route.js");
const { run } = require("./utils/controllerBase.js");
const url = require('url');

Path('readdir')
.then( ({request,response}) => {
    const parsedUrl = url.parse(request.url, true);
    const path = parsedUrl.pathname.endsWith('/') ? parsedUrl.pathname : parsedUrl.pathname + '/';
    const trimmedPath = path.replace(/^\/+|\/+$/g, '');
    const filePath = trimmedPath.toLocaleLowerCase().replace('readdir', '');
    GetFilesOrDir(filePath)
    .then(data => {
        response.statusCode = 200;
        response.setHeader('Content-Type', 'text/plain');
        response.end(JSON.stringify(data));
    })
    .catch(err => {
        response.statusCode = 404;
        response.setHeader('Content-Type', 'text/plain');
        response.end(JSON.stringify(err));
    });

});

Path('file')
.then(
    async ({request,response}) => {
        const parsedUrl = url.parse(request.url, true);
        const path = parsedUrl.pathname;
        const trimmedPath = path.replace(/^\/+|\/+$/g, '');
        const filePath = trimmedPath.toLocaleLowerCase().replace('file', '');
        const {stat,createReadStream} = 
        await GetFile(filePath);

        response.writeHead(200, {
            'Content-Type': 'application/octet-stream',
            'Content-Length': stat.size,
            'Content-Disposition': 'attachment; filename=filename.png'
        });
        
        createReadStream.pipe(response);
        
    });


run();
