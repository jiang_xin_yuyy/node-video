const fs = require('fs');

function writeFile(filePath, fileContent) {
    fs.writeFile(filePath, fileContent, (err) => {
        if (err) throw err;
        console.log('文件已保存！');
    });
}

module.exports ={writeFile} ;
