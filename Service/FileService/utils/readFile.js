const { readdir, stat, readFile, createReadStream } = require('fs');
const fs = require('fs');
const { join } = require('path');

const folderPath = __dirname.replace('utils', 'Mediator');
/**
 * 
 * @param {string} Path 
 * @returns 
 */
function GetFilesOrDir(path) {
    let readdirPath = path ? join(folderPath, path, '/') : folderPath;

    return new Promise((resolve, reject) => {
        //判断是否是文件夹
        if (!fs.existsSync(readdirPath)) {
            return reject([]);
        }
        let urlPaht = readdirPath.replace(folderPath, '') || "/";
        readdir(readdirPath, (err, files) => {
            if (err) {
                console.error(err);
                reject(err);
            }
            if (!files) {
                reject("文件为空");
                return;
            }
            const promises = files.map(file => {
                const filePath = join(readdirPath, file);
                return new Promise((resolve, reject) => {
                    stat(filePath, (err, stats) => {
                        if (err) {
                            console.error(err);
                            reject(err);
                        }

                        resolve({
                            name: file,
                            fileType: stats.isFile() ? file.split('.').pop() : 'dir',
                            path: urlPaht,
                            size: stats.size,
                            createtime: stats.birthtime,
                            updatetime: stats.mtime,
                            isFile: stats.isFile(),
                            isDirectory: stats.isDirectory()
                        });
                    });
                });
            });
            Promise.all(promises).then(data => {
                resolve(data);
            }).catch(err => {
                reject(err);
            });
        });
    });
}
/**
 * 
 * @param {string} file 
 * @returns stream
 */

function GetFile(file) {
    let filePath = join(folderPath, file);
    return new Promise((resolve, reject) => {
        stat(filePath, (err, stats) => {
            if (err) {
                console.error(err);
                reject(err);
            }
            if (stats.isFile()) {
                resolve({
                    stat: stats,
                    createReadStream: createReadStream(filePath)
                });
            } else {
                reject("不是文件");
            }
        });
    });
}


module.exports = { GetFilesOrDir, GetFile };