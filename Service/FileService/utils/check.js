function isAsync(func) {
    const AsyncFunction = (async () => {}).constructor;
    return func instanceof AsyncFunction;
}

module.exports = {isAsync};