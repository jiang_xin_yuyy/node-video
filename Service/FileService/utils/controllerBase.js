const http = require('http');
const { Path, pathList } = require("./route.js");
const { isAsync } = require("./check.js");
const port = 3000;
function run() {
    const server = http.createServer(
        async (req, res) => {
            try {
                const promises = pathList.map(item => {
                    return new Promise((resolve, reject) => {
                        try {
                            if (item(req, res)) {
                                resolve(true);
                            } else {
                                resolve(false);
                            }
                        } catch (error) {
                            reject(error);
                        }
                    });

                });
                const results = await Promise.all(promises);
                let isroute = !results.filter(e=>e).length;
                if (isroute) {
                    res.statusCode = 404;
                    res.end('Not found');
                }
            } catch (error) {
                res.statusCode = 404;
                res.end(JSON.stringify(error));
            }
        }
    )
    // CORS
    server.on('request', (req, res) => {
        res.setHeader('Access-Control-Allow-Origin', 'http://127.0.0.1:5500');
        res.setHeader('Access-Control-Allow-Methods', '*');
        res.setHeader('Access-Control-Allow-Headers', '*');
    });

    server.listen(port, () => {
        console.log('Server running: http://localhost:' + port);
    });
}

module.exports = { run }