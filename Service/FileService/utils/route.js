const http = require('http');
const url = require('url');
const { isAsync } = require('./check.js');
let pathList = [];
/**
 * 
 * @param {IncomingMessage} req 
 * @param {string} actionRoute 
 * @returns 
 */
function Path(actionRoute, func) {
    return new LikePromise((resolve, reject) => {
        pathList.push((req, res) => {
            const parsedUrl = url.parse(req.url, true);
            const path = parsedUrl.pathname.endsWith('/') ? parsedUrl.pathname : parsedUrl.pathname + '/';
            actionRoute = actionRoute.endsWith('/') ? actionRoute : actionRoute + '/';
            actionRoute = actionRoute.startsWith('/') ? actionRoute : '/' + actionRoute;
            if (path.startsWith(actionRoute)) {
                resolve({ request: req, response: res });
                return true;
            } else {
                return false;
            }
        });
    });
}

class LikePromise {
    /**
     * 
     * @param {Function} func 
     */
    constructor(func) {
        const resolve = (data) => {
            if (this.#thenFunc instanceof Function) {
                this.#thenFunc(data);
            } 
        }
        const reject = (err) => {
            if (this.#catchFunc instanceof Function) {
                this.#catchFunc(err);
            } 
        }
        func(resolve, reject);
    }
    get status () {
        return this.#status;
    }
    set status (value) {
        this.#status = value;
        if (this.#status === "fulfilled") {
            
        } else if (this.#status === "rejected") {
            
        }
        if (this.#status !== "pending") {
            this.#finallyFunc();
            this.#status = "pending";
        }
    }
    #status = 'pending';
    #thenFunc;
    #catchFunc;
    #finallyFunc;
    /**
     * 
     * @param {Function} func 
     */
    then(func) {
        this.#thenFunc = func;
        return this;
    }
    /**
     * 
     * @param {Function} func 
     */
    catch(func) {
        this.#catchFunc = func;
        return this;
    }
    /**
    * 
    * @param {Function} func 
    */
    finally(func) {
        this.#finallyFunc = func;
        return this;
    }
}

module.exports = { Path, pathList };