const httpProxy = require('http-proxy');
const url = require('url');
const port = 8000;

httpProxy.createProxyServer({
    target: 'http://localhost:9000'
})
.on('proxyReq', function(proxyReq, req, res, options) {
    proxyReq.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');

    const parsedUrl = url.parse(req.url);
    if (parsedUrl.pathname.toLowerCase().startsWith('/fileservice/')) {
        proxyReq.path = parsedUrl.path.replace(/^\/fileservice/, '');
        proxyReq.setHeader('Host', 'localhost:8001');
    } else if (parsedUrl.pathname.toLowerCase().startsWith('/dbservice/')) {
        proxyReq.path = parsedUrl.path.replace(/^\/dbservice/, '');
        proxyReq.setHeader('Host', 'localhost:8002');
    }
})
.listen(port);

