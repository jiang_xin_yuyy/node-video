using Grpc.Net.Client;
using Microsoft.EntityFrameworkCore;
using System.Reflection;
using UserService.src.infrastructure.AutoDI;
using UserService.src.infrastructure.DbProvider;
using UserService.src.infrastructure.DbService;
using UserService.src.infrastructure.Protos;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.Text;
using Microsoft.Extensions.Options;

var builder = WebApplication.CreateBuilder(args);
var connectionString = builder.Configuration;
// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
//builder.Services.AddScoped(typeof(IUserDbProvider), typeof(UserDbProvider));
builder.Services.AddDbContext<UserServiceContext>();
builder.Services.AddGrpcClient<Greeter.GreeterClient>(o =>
{
    o.Address = new Uri(connectionString.GetConnectionString("GrpcServer"));
});
//jwt
builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(options =>
    {
        options.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuer = true,
            ValidateAudience = true,
            ValidateLifetime = true,
            ValidateIssuerSigningKey = true,
            ValidIssuer = connectionString.GetConnectionString("Jwt:Issuer"),
            ValidAudience = connectionString.GetConnectionString("Jwt:Audience"),
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(connectionString.GetConnectionString("Jwt:SecretKey")))
        };
        options.Events = new JwtBearerEvents
        {
            //OnAuthenticationFailed = context =>
            //{
            //    Console.WriteLine("Authentication failed.");
            //    return Task.CompletedTask;
            //},
            //OnChallenge = context =>
            //{
            //    Console.WriteLine("OnChallenge error", context.Error, context.ErrorDescription);
            //    Console.WriteLine(context.AuthenticateFailure);
            //    return Task.CompletedTask;
            //},
            //OnMessageReceived = context =>
            //{
            //    Console.WriteLine("Received message: " + context.Token);
            //    return Task.CompletedTask;
            //},
        };
    });
//跨域
string cors = "http://localhost:5173";
builder.Services.AddCors(options =>
{
    options.AddPolicy(cors, builder =>
    {
        //允许localhost:3000跨域
        builder.WithOrigins("http://localhost:5173")
        .AllowAnyHeader()
        .AllowAnyMethod();
    });
});

//��ȡ����
var assembly = Assembly.Load("UserService");
builder.Services.AddUserServerInterfaceClient(assembly,
    new string[] {
        "UserService.src.infrastructure.DbProvider"
    , "UserService.src.domain.repository"
    , "UserService.src.domain.service"
    ,"UserService.src.infrastructure.Jwt"
    });

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
app.UseCors(cors);
app.UseRouting();
app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();
