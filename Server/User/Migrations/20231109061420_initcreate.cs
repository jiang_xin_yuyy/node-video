﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace UserService.Migrations
{
    public partial class initcreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "videoRole",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ParentId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Level = table.Column<int>(type: "int", nullable: false),
                    SUID = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_videoRole", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "videoUser",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: true),
                    Phone = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    Password = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RandomCharacters = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    SUID = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_videoUser", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "videoUserRole",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserSUID = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    RoleSUID = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    SUID = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_videoUserRole", x => x.ID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_videoRole_ID",
                table: "videoRole",
                column: "ID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_videoRole_SUID",
                table: "videoRole",
                column: "SUID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_videoUser_Email",
                table: "videoUser",
                column: "Email",
                unique: true,
                filter: "[Email] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_videoUser_Phone",
                table: "videoUser",
                column: "Phone",
                unique: true,
                filter: "[Phone] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_videoUser_SUID",
                table: "videoUser",
                column: "SUID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_videoUserRole_ID",
                table: "videoUserRole",
                column: "ID",
                unique: true);
                
            string RoleSUID = $"{DateTime.Now:yyyyMMdd}_{DateTime.Now.Ticks}_{Guid.NewGuid()}";
            string UserSUID = $"{DateTime.Now:yyyyMMdd}_{DateTime.Now.Ticks}_{Guid.NewGuid()}";
            migrationBuilder.InsertData(
                    table: "videoUser",
                    columns: new[] { "Name", "Email", "Phone", "Password", "RandomCharacters", "SUID" },
                    values: new object[] { "admin", "1304522893@qq.com", null, "8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918", "8c6976e5b5410415", UserSUID}
                );
            migrationBuilder.InsertData(
                table: "videoRole",
                columns: new[] { "Name", "Description", "ParentId", "Level", "SUID" },
                values: new object[] { "admin", "超级管理员", null, 0, RoleSUID }
            );

            migrationBuilder.InsertData(
                table: "videoUserRole",
                columns: new[] { "UserSUID", "RoleSUID", "SUID" },
                values: new object[] { UserSUID, RoleSUID, $"{DateTime.Now:yyyyMMdd}_{DateTime.Now.Ticks}_{Guid.NewGuid()}" }
            );


        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "videoRole");

            migrationBuilder.DropTable(
                name: "videoUser");

            migrationBuilder.DropTable(
                name: "videoUserRole");
        }
    }
}
