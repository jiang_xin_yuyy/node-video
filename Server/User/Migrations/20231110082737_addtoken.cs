﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace UserService.Migrations
{
    public partial class addtoken : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "videoRefreshToken",
                columns: table => new
                {
                    ID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Token = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    UserSUID = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ExpirationDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_videoRefreshToken", x => x.ID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_videoRefreshToken_Token",
                table: "videoRefreshToken",
                column: "Token");

            migrationBuilder.CreateIndex(
                name: "IX_videoRefreshToken_UserSUID",
                table: "videoRefreshToken",
                column: "UserSUID",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "videoRefreshToken");
        }
    }
}
