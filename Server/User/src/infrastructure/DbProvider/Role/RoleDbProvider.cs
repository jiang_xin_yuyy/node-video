using UserService.src.domain.model.Entities;
using UserService.src.infrastructure.DbService;

namespace UserService.src.infrastructure.DbProvider;

public class RoleDbProvider : IRoleDbProvider
{
    private readonly BaseDbProvider<Role> _context;

    public RoleDbProvider(UserServiceContext context){
        _context = new BaseDbProvider<Role>(context);
    }

    public async Task<Role?> GetByIdAsync(string id)
    {
        return await _context.DbSet.FindAsync(id);
    }
}