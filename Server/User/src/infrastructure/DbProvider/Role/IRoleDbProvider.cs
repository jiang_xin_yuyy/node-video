using UserService.src.domain.model.Entities;

namespace UserService.src.infrastructure.DbProvider;

public interface IRoleDbProvider{
    Task<Role?> GetByIdAsync(string id);
}