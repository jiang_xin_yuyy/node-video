using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using UserService.src.domain.model.Entities;
using UserService.src.infrastructure.DbService;

namespace UserService.src.infrastructure.DbProvider
{
    public class BaseDbProvider<T> where T : Base 
    {
        private readonly UserServiceContext _context;
        public BaseDbProvider(UserServiceContext context){
            _context = context;
        }
        public DbContext Db{
            get{
                return _context;
            }
        }
        public DbSet<T> DbSet {
            get{
                return _context.Set<T>();
            }
        }

    }
}