using UserService.src.domain.model.Entities;

namespace UserService.src.infrastructure.DbProvider;

public interface IUserDbProvider
{
    Task<User?> GetByIdAsync(string id);
    Task<IEnumerable<User>> GetAllAsync();
    Task AddAsync(User user);
    Task UpdateAsync(User user);
    Task DeleteAsync(string id);
}