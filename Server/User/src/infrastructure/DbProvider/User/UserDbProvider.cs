using Microsoft.EntityFrameworkCore;
using UserService.src.domain.model.Entities;
using UserService.src.infrastructure.DbService;

namespace UserService.src.infrastructure.DbProvider;

public class UserDbProvider:BaseDbProvider<User>,IUserDbProvider {
    public UserDbProvider(UserServiceContext context) : base(context)
    {
       
    }

    public  async Task<User?> GetByIdAsync(string id)
        {
            return await DbSet.FindAsync(id);
        }

        public async Task<IEnumerable<User>> GetAllAsync()
        {
        
            return await DbSet.ToListAsync();
        }

        public async Task AddAsync(User user)
        {
            await DbSet.AddAsync(user);
            await Db.SaveChangesAsync();
        }

        public async Task UpdateAsync(User user)
        {
            DbSet.Update(user);
            await Db.SaveChangesAsync();
        }

        public async Task DeleteAsync(string id)
        {
            var user = await DbSet.FindAsync(id);
            if (user != null)
            {
                DbSet.Remove(user);
                await Db.SaveChangesAsync();
            }
        }
}