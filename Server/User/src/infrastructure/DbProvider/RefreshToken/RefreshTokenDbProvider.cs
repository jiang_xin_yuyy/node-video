using Microsoft.EntityFrameworkCore;
using UserService.src.domain.model.Entities;
using UserService.src.infrastructure.DbService;

namespace UserService.src.infrastructure.DbProvider;

public class RefreshTokenDbProvider : IRefreshTokenDbProvider
{
    private readonly UserServiceContext _context;
    private readonly DbSet<RefreshToken> _refreshToken;
    public RefreshTokenDbProvider(UserServiceContext context){
        _context = context;
        _refreshToken = _context.RefreshToken;
    }
    
    public async Task<bool> CreateRefreshToken(RefreshToken refreshToken)
    {
        await _refreshToken.AddAsync(refreshToken);
        return await _context.SaveChangesAsync()>0? true:false;
    }

    public async Task<bool> DeleteRefreshToken(string userSUID)
    {
        return await _refreshToken.Where(x => x.UserSUID == userSUID).ExecuteDeleteAsync()>0;
    }

    public async Task<RefreshToken?> GetUserBySUIDAsync(string userSUID)
    {
        return await _refreshToken.Where(x =>x.UserSUID == userSUID).SingleOrDefaultAsync();
    } 
}