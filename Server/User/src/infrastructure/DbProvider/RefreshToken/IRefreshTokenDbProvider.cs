using UserService.src.domain.model.Entities;

namespace UserService.src.infrastructure.DbProvider;

public interface IRefreshTokenDbProvider
{
    public Task<bool> CreateRefreshToken(RefreshToken refreshToken);
    public Task<bool> DeleteRefreshToken(string userSUID);
    Task<RefreshToken?> GetUserBySUIDAsync(string userSUID);
}