using System.Reflection;

namespace UserService.src.infrastructure.AutoDI
{
    public static class Finder
    {

        public static IServiceCollection AddUserServerInterfaceClient(this IServiceCollection services)
        {
            services.FindAndRegister();
            return services;
        }
        public static IServiceCollection AddUserServerInterfaceClient(this IServiceCollection services, Assembly assembly)
        {
            assemblys = new Assembly[] { assembly };
            services.FindAndRegister();
            return services;
        }
        public static IServiceCollection AddUserServerInterfaceClient(this IServiceCollection services, Assembly[] assemblies)
        {
            assemblys = assemblies;
            services.FindAndRegister();
            return services;
        }
        public static IServiceCollection AddUserServerInterfaceClient(this IServiceCollection services, Assembly assembly, string @namespace)
        {
            assemblys = new Assembly[] { assembly };
            var types = GetInterfaces(@namespace);
            foreach (var @interface in types)
            {
                var implementations = GetImplementations(@interface);
                Register(services, @interface, implementations);
            }
            return services;
        }
        public static IServiceCollection AddUserServerInterfaceClient(this IServiceCollection services, Assembly assembly, string[] @namespaces)
        {
            assemblys = new Assembly[] { assembly };
            var types = GetInterfaces(@namespaces);
            foreach (var @interface in types)
            {
                var implementations = GetImplementations(@interface);
                Register(services, @interface, implementations);
            }
            return services;
        }
        public static IServiceCollection AddUserServerInterfaceClient(this IServiceCollection services, string @namespace)
        {
            var types = GetInterfaces(@namespace);
            foreach (var @interface in types)
            {
                var implementations = GetImplementations(@interface);
                Register(services, @interface, implementations);
            }
            return services;
        }
        public static IServiceCollection AddUserServerInterfaceClient(this IServiceCollection services, string[] @namespaces)
        {
            var types = GetInterfaces(@namespaces);
            foreach (var @interface in types)
            {
                var implementations = GetImplementations(@interface);
                Register(services, @interface, implementations);
            }
            return services;
        }
        private static void FindAndRegister(this IServiceCollection services)
        {
            var types = assemblys.SelectMany(x => x.GetTypes());
            var interfaces = GetInterfaces();
            foreach (var @interface in types)
            {
                var implementations = GetImplementations(@interface);
                Register(services, @interface, implementations);
            }
        }

        private static Assembly[] _assemblys = AppDomain.CurrentDomain.GetAssemblies().Where(a => !a.FullName.StartsWith("Microsoft.") && !a.FullName.StartsWith("System.")).ToArray();
        private static Assembly[] assemblys
        {
            get
            {
                return _assemblys;
            }
            set
            {
                _assemblys = value;
            }
        } //= AppDomain.CurrentDomain.GetAssemblies();
        // new Assembly[] { Assembly.Load("UserService") };


        //获取所有接口
        private static List<Type> GetInterfaces()
        {
            return assemblys.SelectMany(x => x.GetTypes()).Where(x => x.IsInterface).ToList();
        }
        //根据命名空间获取所有接口
        private static List<Type> GetInterfaces(string @namespace)
        {
            return assemblys.SelectMany(x => x.GetTypes()).Where(x => x.IsInterface && x.Namespace == @namespace).ToList();
        }
        //根据命名空间的集合获取所有接口
        private static List<Type> GetInterfaces(string[] @namespaces)
        {
            return assemblys.SelectMany(x => x.GetTypes()).Where(x => x.IsInterface && @namespaces.Contains(x.Namespace)).ToList();
        }
        //根据接口获取所有实现类
        private static List<Type> GetImplementations(Type @interface)
        {
            return assemblys.SelectMany(x => x.GetTypes()).Where(x => x.IsClass && !x.IsAbstract && @interface.IsAssignableFrom(x)).ToList();
        }
        //传入接口，实现类集合，注册到容器
        private static void Register(IServiceCollection services, Type @interface, List<Type> implementations)
        {
            foreach (var implementation in implementations)
            {
                Console.WriteLine("接口：" + @interface);
                Console.WriteLine("实现：" + implementation);
                services.AddScoped(@interface, implementation);
            }
        }

    }
}