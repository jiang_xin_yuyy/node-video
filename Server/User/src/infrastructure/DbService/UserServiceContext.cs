using Microsoft.EntityFrameworkCore;
using UserService.src.domain.model.Entities;
using Microsoft.Extensions.Configuration;

namespace UserService.src.infrastructure.DbService
{
    public class UserServiceContext : DbContext
    {
        private readonly IConfiguration _configuration;
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<UserRole> UserRole { get; set; }

        public DbSet<RefreshToken> RefreshToken { get; set; }

        public UserServiceContext(IConfiguration configuration)
        {
            _configuration = configuration;
            Users = Set<User>();
            Roles = Set<Role>();
            UserRole = Set<UserRole>();
            RefreshToken = Set<RefreshToken>();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        => options.UseSqlServer(_configuration.GetConnectionString("UserDb"));
        //处理表名
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //数据量大的情况下, 会根据年月日进行分表
            modelBuilder.Entity<User>(db=>{
                db.ToTable("videoUser"); 
                db.Property(x=>x.Email).HasMaxLength(300);
                db.HasIndex(x=>x.Email).IsUnique();
                db.Property(x=>x.Phone).HasMaxLength(20);
                db.HasIndex(x=>x.Phone).IsUnique();
                db.HasIndex(x=>x.SUID).IsUnique();
                db.Property(x=>x.ID).ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<Role>(db=>{
                db.ToTable("videoRole");
                db.HasIndex(x=>x.SUID).IsUnique();
                db.HasIndex(x=>x.ID).IsUnique();
                db.Property(x=>x.ID).ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<UserRole>(db=>{
                db.ToTable("videoUserRole");
                db.HasIndex(x=>x.ID).IsUnique();
                db.Property(x=>x.ID).ValueGeneratedOnAdd();
            });
            
            modelBuilder.Entity<RefreshToken>(db=>{
                db.ToTable("videoRefreshToken");
                db.HasIndex(x=>x.UserSUID).IsUnique();
                db.HasIndex(x=>x.Token);
            });

            // string RoleSUID = $"{DateTime.Now:yyyyMMdd}_{DateTime.Now.Ticks}_{Guid.NewGuid()}";
            // string UserSUID = $"{DateTime.Now:yyyyMMdd}_{DateTime.Now.Ticks}_{Guid.NewGuid()}";
            // migrationBuilder.InsertData(
            //         table: "videoUser",
            //         columns: new[] { "Name", "Email", "Phone", "Password", "RandomCharacters", "SUID" },
            //         values: new object[] { "admin", "1304522893@qq.com", null, "8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918", "8c6976e5b5410415", UserSUID}
            //     );
            // migrationBuilder.InsertData(
            //     table: "videoRole",
            //     columns: new[] { "Name", "Description", "ParentId", "Level", "SUID" },
            //     values: new object[] { "admin", "超级管理员", null, 0, RoleSUID }
            // );

            // migrationBuilder.InsertData(
            //     table: "videoUserRole",
            //     columns: new[] { "UserSUID", "RoleSUID", "SUID" },
            //     values: new object[] { UserSUID, RoleSUID, $"{DateTime.Now:yyyyMMdd}_{DateTime.Now.Ticks}_{Guid.NewGuid()}" }
            // );




        }
    }
}