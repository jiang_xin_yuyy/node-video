using System.Security.Cryptography;
using System.Text;

namespace UserService.src.infrastructure.Crypt;

public static class Encryption
{
    public static string Md5(string str)
    {
        using var md5 = MD5.Create();
        var result = md5.ComputeHash(Encoding.UTF8.GetBytes(str));
        var strResult = BitConverter.ToString(result);
        return strResult;
    }

    //sha256加密
    public static string Sha256(string str)
    {
        using var sha256 = SHA256.Create();
        var result = sha256.ComputeHash(Encoding.UTF8.GetBytes(str));
        var strResult = BitConverter.ToString(result);
        return strResult;
    }

    //sha512加密
    public static string Sha512(string str)
    {
        using var sha512 = SHA512.Create();
        var result = sha512.ComputeHash(Encoding.UTF8.GetBytes(str));
        var strResult = BitConverter.ToString(result);
        return strResult;
    }

    //sha1加密
    public static string Sha1(string str)
    {
        using var sha1 = SHA1.Create();
        var result = sha1.ComputeHash(Encoding.UTF8.GetBytes(str));
        var strResult = BitConverter.ToString(result);
        return strResult;
    }

    //hmacsha256加密
    public static string HmacSha256(string str, string key)
    {
        using var hmacsha256 = new HMACSHA256(Encoding.UTF8.GetBytes(key));
        var result = hmacsha256.ComputeHash(Encoding.UTF8.GetBytes(str));
        var strResult = BitConverter.ToString(result);
        return strResult;
    }

    //hmacsha512加密
    public static string HmacSha512(string str, string key)
    {
        using var hmacsha512 = new HMACSHA512(Encoding.UTF8.GetBytes(key));
        var result = hmacsha512.ComputeHash(Encoding.UTF8.GetBytes(str));
        var strResult = BitConverter.ToString(result);
        return strResult;
    }

    //hmacsha1加密
    public static string HmacSha1(string str, string key)
    {
        using var hmacsha1 = new HMACSHA1(Encoding.UTF8.GetBytes(key));
        var result = hmacsha1.ComputeHash(Encoding.UTF8.GetBytes(str));
        var strResult = BitConverter.ToString(result);
        return strResult;
    }
    
    //hmacmd5加密
    public static string HmacMd5(string str, string key)
    {
        using var hmacmd5 = new HMACMD5(Encoding.UTF8.GetBytes(key));
        var result = hmacmd5.ComputeHash(Encoding.UTF8.GetBytes(str));
        var strResult = BitConverter.ToString(result);
        return strResult;
    }

}