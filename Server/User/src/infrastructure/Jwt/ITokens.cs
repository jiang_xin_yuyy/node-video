using System.Security.Claims;
using UserService.src.domain.model.Entities;

namespace UserService.src.infrastructure.Jwt;

public interface ITokens
{
    public string GenerateToken(Claim[] claims);
    string GenerateToken(User user);
    public string GenerateRefreshToken(string SUID);
    public Task<string>  RefreshToken(string userSUID, string accessToken);
    /// <summary>
    /// ����refreshToken
    /// </summary>
    /// <param name="refreshToken"></param>
    /// <returns></returns>
    Task<bool> SaveRefreshToken(RefreshToken refreshToken);
    /// <summary>
    /// ɾ��refreshToken
    /// </summary>
    /// <param name="userSUID"></param>
    /// <returns></returns>
    Task<bool> DeleteRefreshToken(string userSUID);
    /// <summary>
    /// ��ȡrefreshToken
    /// </summary>
    /// <param name="userSUID"></param>
    /// <returns></returns>
    Task<RefreshToken?> GetRefreshToken(string userSUID);
}