using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using UserService.src.domain.model.DTO;
using UserService.src.domain.model.Entities;
using UserService.src.infrastructure.DbProvider;

namespace UserService.src.infrastructure.Jwt
{
    public class Tokens : ITokens
    {
        private readonly string SecretKey;
        private readonly string Issuer;
        private readonly string Audience;
        private readonly IRefreshTokenDbProvider _refreshTokenDbProvider;
        public Tokens(IConfiguration configuration, IRefreshTokenDbProvider refreshTokenDbProvider)
        {
            SecretKey = configuration.GetConnectionString("Jwt:SecretKey");
            Issuer = configuration.GetConnectionString("Jwt:Issuer");
            Audience = configuration.GetConnectionString("Jwt:Audience");
            _refreshTokenDbProvider = refreshTokenDbProvider;
        }
        //生成token
        public string GenerateToken(Claim[] claims)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(SecretKey);
            
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.UtcNow.AddHours(8), // 设置 token 的过期时间
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature),
                Issuer = Issuer,
                Audience = Audience
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        public string GenerateToken(User user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(SecretKey);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim("SUID",user.SUID),
                    new Claim("UserName",string.IsNullOrEmpty(user.Name)?"":user.Name),
                    new Claim("Email",string.IsNullOrEmpty(user.Email)?"":user.Email),
                    new Claim("Phone",string.IsNullOrEmpty(user.Phone)?"":user.Phone)
                }),
                Expires = DateTime.UtcNow.AddHours(8), // 设置 token 的过期时间
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature),
                Issuer = Issuer,
                Audience = Audience
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
        
        public string GenerateRefreshToken(string SUID)
        {
            var randomNumber = new byte[32];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomNumber);
                var refreshToken = Convert.ToBase64String(randomNumber);
                var refreshTokenInfo =  _refreshTokenDbProvider.GetUserBySUIDAsync(SUID);
                return refreshToken;
            }
        }
        //保存refreshToken
        public  Task<bool> SaveRefreshToken(RefreshToken refreshToken)
        {
            return  _refreshTokenDbProvider.CreateRefreshToken(refreshToken);
        }
        //删除refreshToken
        public  Task<bool> DeleteRefreshToken(string userSUID)
        {
           return  _refreshTokenDbProvider.DeleteRefreshToken(userSUID);
        }
        public Task<RefreshToken?> GetRefreshToken(string userSUID)
        {
            return  _refreshTokenDbProvider.GetUserBySUIDAsync(userSUID);
        }

        //通过refreshToken和token获取新的token
        public async Task<string> RefreshToken(string refreshToken, string token)
        {
            var claims = GetUserByToken(token);
            if (claims != null)
            {
                var SUID = claims.Where(x=>x.Type =="SUID").First().Value;
                var refreshTokenInfo = await _refreshTokenDbProvider.GetUserBySUIDAsync(SUID);
                var validateRefreshToken =  ValidateRefreshToken(refreshToken, refreshTokenInfo);               
                if (validateRefreshToken)
                {
                    var refresh = GenerateRefreshToken(SUID);
                    await _refreshTokenDbProvider.DeleteRefreshToken(SUID);
                    await _refreshTokenDbProvider.CreateRefreshToken(new RefreshToken
                    {
                        UserSUID = SUID,
                        Token = refresh,
                        ExpirationDate = DateTime.UtcNow.AddDays(1)
                    });
                    var res =
                        new {
                            refreshToken = refresh,
                            token = GenerateToken(claims)
                        };
                    return JsonConvert.SerializeObject(res);
                }
            }

            return "";
        }

        //验证token是否有效
        public ClaimsPrincipal? ValidateToken(string token)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(SecretKey);
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true, // 验证签名
                IssuerSigningKey = new SymmetricSecurityKey(key),
                ValidateIssuer = false, // 不验证发布者
                ValidateAudience = false, // 不验证订阅者
                ValidateLifetime = false, // 验证生命周期
                ClockSkew = TimeSpan.Zero // 时钟偏差
            };
            var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out var validatedToken);
            if (principal != null)
            {
                var jwtToken = validatedToken as JwtSecurityToken;
                if (jwtToken == null || !jwtToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
                {
                    return null;
                }
                return principal;
            }
            return null;
        }

        //验证refreshToken是否有效
        public bool ValidateRefreshToken(string refreshToken, RefreshToken? refreshTokenInfo)
        {
            
            if (refreshTokenInfo !=null)
            {
                if (refreshTokenInfo.Token == refreshToken && refreshTokenInfo.ExpirationDate > DateTime.UtcNow)
                {
                    return true;
                }else{
                    return false;
                }
            }
            return false;
        }

        //通过token获取用户信息
        public Claim[]? GetUserByToken(string token)
        {
            var principal = ValidateToken(token);
            if (principal != null)
            {
                return principal.Claims.ToArray();
            }
            return null;
        }
    }
}