﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UserService.src.domain.model.DTO;
using UserService.src.domain.service;

namespace UserService.src.domain.Controllers
{
    [Route("/[controller]/[action]")]
    [ApiController]
    public class UserController : VideoControllerBase
    {
        private readonly IUserService userService;
        public UserController(IUserService userService)
        {
            this.userService = userService;
        }

        [HttpPost]
        public async Task<ObjectResult> Login(UserDTO userDTO)
        {
            try
            {
                var res = await userService.Login(userDTO);
                return SuccessResult(res,"登录成功");
            }
            catch (Exception e)
            {
                return functionErrorResult(e,e.Message);
            }

        }

        [HttpPost]
        public async Task<ObjectResult> Register([FromBody] UserDTO userDTO)
        {
            try
            {
                var res = await userService.Register(userDTO);
                return SuccessResult(res);
            }
            catch (Exception e)
            {
                return functionErrorResult(e,e.Message);
            }
        }
        [Authorize]
        [HttpGet]
        public async Task<ObjectResult> RefershToken(){
            try
            {
                var token = Request.Headers["Authorization"].ToString().Split(" ")[1];
                var refreshToken = Request.Headers["RefreshToken"].ToString();
                var res = await userService.RefreshToken(token,refreshToken);
                return SuccessResult(res);
            }
            catch (Exception e)
            {
                return functionErrorResult(e,e.Message);
            }
        }

    }
}
