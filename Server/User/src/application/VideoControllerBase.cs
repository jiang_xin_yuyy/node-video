﻿
namespace Microsoft.AspNetCore.Mvc
{
    public class VideoControllerBase : Controller
    {
        enum status
        {
            success,
            warn,
            systemError,
            functionError
        }
        private class ResultModel
        {
          public  ResultModel(string message,status status)
            {
                this.Message = message;
                this.Status = status.ToString();
            }
          public object? Data { get; set; }
          public string Message { get; set; }
          public string Status { get; set; }
        }
        /// <summary>
        /// 成功的返回值
        /// </summary>
        /// <param name="data"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        [NonAction]
        public OkObjectResult SuccessResult(object data,string message= "Success")
        {
            var result = new  ResultModel(message,status.success);
            result.Data = data;
            return new OkObjectResult(result);
        }
        /// <summary>
        /// 返回警告内容
        /// </summary>
        /// <param name="data"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        [NonAction]
        public OkObjectResult WarnResult(object data, string message = "Warn")
        {
            var result = new ResultModel(message, status.warn);
            result.Data = data;
            return new OkObjectResult(result);
        }
        /// <summary>
        /// 方法错误，在业务逻辑上的错误
        /// </summary>
        /// <param name="data"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        [NonAction]
        public OkObjectResult functionErrorResult(object data, string message = "functionError")
        {
            var result = new ResultModel(message, status.functionError);
            result.Data = data;
            return new OkObjectResult(result);
        }
        /// <summary>
        /// 系统错误，出现了一些不可控制的，不可预测的错误
        /// </summary>
        /// <param name="data"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        [NonAction]
        public OkObjectResult systemErrorResult(object data, string message = "systemError")
        {
            var result = new ResultModel(message, status.systemError);
            result.Data = data;
            return new OkObjectResult(result);
        }

    }
}


