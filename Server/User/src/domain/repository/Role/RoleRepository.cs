using UserService.src.domain.model.Entities;
using UserService.src.infrastructure.DbProvider;

namespace UserService.src.domain.repository;

public class RoleRepository : IRoleRepository
{
    private readonly IRoleDbProvider _roleDbProvider;
    public RoleRepository(IRoleDbProvider roleDbProvider)
    {
        _roleDbProvider = roleDbProvider;
    }

    public async Task AddAsync(Role role)
    {
       await ValidateRoleAsync(role);
    }

    public Task DeleteAsync(string id)
    {
        throw new NotImplementedException();
    }

    public Task<IEnumerable<Role>> GetAllAsync()
    {
        throw new NotImplementedException();
    }

    public Task<Role?> GetByIdAsync(string id)
    {
        return  _roleDbProvider.GetByIdAsync(id);
    }

    public async Task UpdateAsync(Role role)
    {
        await ValidateRoleAsync(role);
    }

    public async Task ValidateRoleAsync(Role role)
    {
        if (role.Level < 0)
        {
            throw new Exception("角色等级不能小于0！");
        }
        if (role.Level > 0 && string.IsNullOrEmpty(role.ParentId))
        {
            throw new Exception("角色等级大于0时，父级角色不能为空！");
        }

        if (!string.IsNullOrEmpty(role.ParentId))
        {
            var queryRole = await _roleDbProvider.GetByIdAsync(role.ParentId);
            if (queryRole == null)
            {
                throw new Exception("父级角色不存在！");
            }
        }
    }
}