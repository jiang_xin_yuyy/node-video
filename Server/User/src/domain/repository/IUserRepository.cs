using UserService.src.domain.model.Entities;
namespace UserService.src.domain.repository;
public interface IUserRepository
{
    Task<User?> GetBySUIDAsync(string SUID);

    Task<User?> GetByEmailAsync(string email);
    Task<User?> GetByPhoneAsync(string Phone);
    Task<IEnumerable<User>> GetAllAsync();
    Task<User?> AddAsync(User user);
    Task UpdateAsync(User user);
    Task DeleteAsync(string id);
}