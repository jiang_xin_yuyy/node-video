using Microsoft.EntityFrameworkCore;
using UserService.src.domain.model.Entities;
using UserService.src.infrastructure.DbService;

namespace UserService.src.domain.repository
{
    public class UserRepository : IUserRepository
    {
        private readonly UserServiceContext _context;

        public UserRepository(UserServiceContext context)
        {
            _context = context;
        }

        public  async Task<User?> GetBySUIDAsync(string SUID)
        {
            return await _context.Users.Where(user => user.SUID == SUID).SingleOrDefaultAsync();
        }

        public async Task<User?> GetByEmailAsync(string email)
        {
            return await _context.Users.Where(user => user.Email == email).SingleOrDefaultAsync();
        }

        public async Task<User?> GetByPhoneAsync(string Phone)
        {
            return await _context.Users.Where(user => user.Phone == Phone).SingleOrDefaultAsync();
        }

        public async Task<IEnumerable<User>> GetAllAsync()
        {
            return await _context.Users.ToListAsync();
        }

        public async Task<User?> AddAsync(User user)
        {
            await _context.Users.AddAsync(user);
            await _context.SaveChangesAsync();
            return await _context.Users.Where(x=>x.SUID == user.SUID).SingleOrDefaultAsync();
        }

        public async Task UpdateAsync(User user)
        {
            _context.Users.Update(user);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(string id)
        {
            var user = await _context.Users.FindAsync(id);
            if (user != null)
            {
                _context.Users.Remove(user);
                await _context.SaveChangesAsync();
            }
        }
    }
}