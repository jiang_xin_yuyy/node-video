using Newtonsoft.Json;
using UserService.src.domain.model.DTO;
using UserService.src.domain.model.Entities;
using UserService.src.domain.repository;
using UserService.src.infrastructure.Crypt;
using UserService.src.infrastructure.Jwt;

namespace UserService.src.domain.service
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly ITokens _tokens;
        public UserService(IUserRepository userRepository, ITokens tokens)
        {
            _userRepository = userRepository;
            _tokens = tokens;
        }

        public async Task<string> Login(UserDTO userDTO)
        {
            User? user = await _userRepository.GetByEmailAsync(userDTO.Email);
            if (user == null)
            {
                return "用户不存在";
            }else
            {
                var pwd = userDTO.Password + user.RandomCharacters;
                var hashPwd = Encryption.Sha256(pwd);
                if (hashPwd == user.Password)
                {
                    await _tokens.DeleteRefreshToken(user.SUID);
                    var res = new {
                        token = _tokens.GenerateToken(user),
                        refreshToken = _tokens.GenerateRefreshToken(user.SUID)
                    };
                    await _tokens.SaveRefreshToken(new RefreshToken {
                       UserSUID = user.SUID,
                     Token = res.refreshToken,
                     ExpirationDate = System.DateTime.UtcNow.AddDays(1)
                    });
                    return JsonConvert.SerializeObject(res);
                }
                else
                {
                    return "密码错误";
                }
            }
        }

        public async Task<bool>  Register(UserDTO userDTO)
        {
            var user = new User();
            user.Name = userDTO.Name;
            user.SetPassword(userDTO.Password);
            user.SetEmailandPhone(userDTO.Email, userDTO.Phone);
            var registerUser = await _userRepository.AddAsync(user);
            return registerUser != null ? true : false;
        }
        
        public  Task<string> RefreshToken(string token, string refreshToken){
            return  _tokens.RefreshToken(refreshToken,token);
        }
    }
}