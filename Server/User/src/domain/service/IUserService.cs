using UserService.src.domain.model.DTO;

namespace UserService.src.domain.service;

public interface IUserService
{   
    //登录
    public Task<string> Login(UserDTO userDTO);
    //注册
    public Task<bool> Register(UserDTO userDTO);

    public Task<string> RefreshToken(string token, string refreshToken);
}