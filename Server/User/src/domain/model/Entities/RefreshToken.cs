namespace UserService.src.domain.model.Entities
{
    public record RefreshToken
    {
        public Guid ID { get; set; }
        public string Token { get; set; }
        public string UserSUID { get; set; }
        public DateTime ExpirationDate { get; set; }
    }
}