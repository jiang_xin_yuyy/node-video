﻿namespace UserService.src.domain.model.Entities
{
    public record Base
    {
        /// <summary>
        /// 系统唯一标识
        /// </summary>
        public int ID { get; private set; }

        /// <summary>
        /// 业务唯一标识
        /// </summary>
        public string SUID { get; private set; }

        public Base()
        {
            // 业务唯一标识: 年月日_+时间戳_+guid
            // 数据量大的情况下, 会根据年月日进行分表, 以便提高查询效率
            string SUID = $"{DateTime.Now:yyyyMMdd}_{DateTime.Now.Ticks}_{Guid.NewGuid()}";
            this.SUID = SUID;
           
        }
    }
}
