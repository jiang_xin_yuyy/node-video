﻿using System.Security.Cryptography;
using UserService.src.infrastructure.Crypt;

namespace UserService.src.domain.model.Entities
{
    public record User : Base
    {
        public string? Name { get; set; }
        private string? email;
        public string? Email
        {
            get { return email; }
            set
            {
                if (string.IsNullOrEmpty(value) && string.IsNullOrEmpty(Phone))
                {
                    throw new Exception("邮箱和手机号码至少保证一个不能为空！");
                }
                email = value;
            }
        }
        private string? phone;
        public string? Phone
        {
            get { return phone; }
            set
            {
                if (string.IsNullOrEmpty(value) && string.IsNullOrEmpty(Email))
                {
                    throw new Exception("邮箱和手机号码至少保证一个不能为空！");
                }
                phone = value;
            }
        }
        public string? Password { get; private set; }
        public string?  RandomCharacters { get; private set; }
        private void Check()
        {
            if (string.IsNullOrEmpty(Email) && string.IsNullOrEmpty(Phone))
            {
                throw new Exception("邮箱和手机号码至少保证一个不能为空！");
            }
        }

        private static string GenerateSalt(int size)
        {
            // 创建一个字节数组来存储盐
            byte[] saltBytes = new byte[size];
            // 用一个安全的随机数来填充字节数组
            RandomNumberGenerator.Fill(saltBytes);
            // 将字节数组转换为字符串
            return Convert.ToBase64String(saltBytes);
        }

        public void SetPassword(string password)
        {
            if (string.IsNullOrEmpty(password))
            {
                throw new Exception("密码不能为空！");
            }
            RandomCharacters = GenerateSalt(16);
            Password = Encryption.Sha256(password + RandomCharacters);
        }

        public void SetEmailandPhone(string email, string phone)
        {
            if (string.IsNullOrEmpty(email) && string.IsNullOrEmpty(phone))
            {
                throw new Exception("邮箱和手机号码至少保证一个不能为空！");
            }
            this.email = email;
            this.phone = phone;
        }

    }
}
