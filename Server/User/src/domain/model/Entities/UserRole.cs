namespace UserService.src.domain.model.Entities;

public record UserRole : Base
{
    public string UserSUID { get; set; }
    public string RoleSUID { get; set; }

    public UserRole(User user, Role role)
    {
        UserSUID = user.SUID;
        RoleSUID = role.SUID;
    }

    public UserRole()
    {

    }
}