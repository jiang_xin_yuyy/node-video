namespace UserService.src.domain.model.Entities;

public record Role:Base {
    public string? Name { get; set; }
    public string? Description { get; set; }
    public string? ParentId { get; set; }
    public int Level { get; set; }
    public Role(string name,int level = 0, string? parentId = null)
    {
        if (level <0)
        {
            throw new Exception("角色等级不能小于0！");
        }
        if (level > 0 && string.IsNullOrEmpty(parentId))
        {
            throw new Exception("角色等级大于0时，父级角色不能为空！");
        }
        this.Name = name;
        this.Level = level;
        this.ParentId = parentId;
    }
}