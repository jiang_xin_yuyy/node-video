
namespace UserService.src.domain.model.DTO
{
    public record UserDTO 
    {
        public string? SUID { get; set; }
        public string? Name { get; set; }
        public string? Email { get; set; }
        public string? Phone {get;set;}
        public string? Password { get; set; }
    }
}