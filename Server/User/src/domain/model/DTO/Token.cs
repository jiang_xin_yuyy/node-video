namespace UserService.src.domain.model.DTO
{
    public record Token 
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
    }
}