## 需要的包
Microsoft.EntityFrameworkCore
Microsoft.EntityFrameworkCore.SqlServer
Microsoft.EntityFrameworkCore.Design
 Grpc.Net.Client
 Google.Protobuf

 ## 工具
  Grpc.Tools
  dotnet-ef

  ## 注意事项
  每次更改完*.proto后执行dotnet build 以免出现一些不可预测的错误