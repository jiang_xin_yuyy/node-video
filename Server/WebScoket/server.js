'use strict';
const WebSocket = require('ws');
const port = process.env.PORT || 1337;
const wss = new WebSocket.Server({ port: port });
console.log("runing server port:" + port);
const serverData = {
    get users() {
        if (this.isOpenWsChange) {
            this._users = [];
            wss.clients.forEach((item) => {
                this._users.push(item.user);
            });
        }
        return this._users;
    },
    _users: [],
    isOpenWsChange: false
}

const routerAction = [
    {
        likeUrl: '/inituser',
        action: (ws, data) => {
            ws.user = data.data.user;
            //每次用户变化都要通知所有用户
            serverData.isOpenWsChange = true;
            let sendData = JSON.stringify({
                likeUrl: '/webrtc/getuser',
                data: serverData.users
            });
            wss.clients.forEach((item) => {
                item.send(sendData);
            });
        }
    },
    {
        likeUrl: '/webrtc/getuser',
        action: (ws, data) => {
            data.data =serverData.users;
            ws.send(JSON.stringify(data));
        }
    },
    {
        likeUrl: "/webrtc/consult",
        action: (ws, data) => {
            let toUser = data.data.toUser;
            let toWs = selectTargetWs(toUser);
            if (toWs) {
                toWs.send(JSON.stringify(data));
            } else {
                data.data = {
                    type: "error",
                    msg: "对方不在线"
                };
                ws.send(JSON.stringify(data));
            }
        }
    }
];
function findAndExecuteAction(ws, data) {
    for (let index = 0; index < routerAction.length; index++) {
        const element = routerAction[index];
        if (element.likeUrl === data.likeUrl) {
              element.action(ws,data);
              break;
        }
    }
}


function selectTargetWs(toUser) {
    let allWs = [...wss.clients];
    for (var i = 0; i < allWs.length; i++) {
        let e = allWs[i];
        if (e.user === toUser) {
            return e;
        }
    }
}


wss.on('connection', function connection(ws) {
    ws.on('message', function incoming(message) {
        let data = JSON.parse(message.toString());
        findAndExecuteAction(ws, data);
    });

    ws.on('close', function close() {
        serverData.isOpenWsChange = true;
        let sendData = JSON.stringify({
            likeUrl: '/webrtc/getuser',
            data: serverData.users
        });
        wss.clients.forEach((item) => {
            item.send(sendData);
        });
    });

});
