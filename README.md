### 目录结构


## 环境准备
安装SQLite3
 如果您在Windows上运行此命令，请确保您的计算机上已经安装了构建工具和Python环境。您可以使用以下命令来安装它们：
 npm install --global --production windows-build-tools
npm install --global node-gyp

### 安装node-windows
1. 全局安装node-windows
```
npm install -g node-windows
```
2. 然后，在项目根目录中，运行：
```
npm link node-windows
```

### 运行步骤